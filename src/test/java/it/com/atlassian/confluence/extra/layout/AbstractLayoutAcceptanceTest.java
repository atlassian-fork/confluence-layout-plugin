package it.com.atlassian.confluence.extra.layout;

import com.atlassian.confluence.AbstractConfluenceAcceptanceTest;
import com.atlassian.confluence.it.User;

public class AbstractLayoutAcceptanceTest extends AbstractConfluenceAcceptanceTest {
    protected static final String SECTION_MACRO_NAME = "section";

    protected static final String COLUMN_MACRO_NAME = "column";

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        logins().logInAs(User.ADMIN);
        rpc.createSpace(TEST_SPACE);
    }
}
