package com.atlassian.confluence.extra.layout;

import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.v2.macro.basic.validator.MacroParameterValidationException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static com.atlassian.renderer.v2.RenderMode.ALL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ColumnMacroTestCase {
    @Mock
    private RenderContext renderContext;

    private Map macroParameters;

    private ColumnMacro columnMacro;

    @Before
    public void setUp() {
        columnMacro = new ColumnMacro();
        macroParameters = new HashMap();
    }

    @Test
    public void testRenderForWysiwyg() throws MacroException {
        final String body = "Macro body text";

        final String expectedHtml = "<div class=\"columnMacro\">" +
                body + "</div>";
        final String actualHtml = columnMacro.execute(macroParameters, body, renderContext);
        assertEquals(expectedHtml, actualHtml);

        verify(renderContext, times(0)).isRenderingForWysiwyg();
    }

    @Test
    public void testColumnMacroWithInvalidWidth() throws Exception {
        Map params = new HashMap();
        params.put("width", "\"><script>alert('not good')</script><br x=\"");
        try {
            columnMacro.execute(params, "foo", new RenderContext());
            fail("No exception thrown");
        } catch (MacroParameterValidationException e) {
            // ignore expected exception
        }
    }

    @Test
    public void testColumnMacroWithValidWidth() throws MacroException {
        Map params = new HashMap();
        params.put("width", "50%");
        assertEquals("<div class=\"columnMacro\" style=\"width:50%;min-width:50%;max-width:50%;\">foo</div>", columnMacro.execute(params, "foo", new RenderContext()));
    }

    @Test
    public void testBodyIsRenderedToHtml() {
        assertEquals(ALL, columnMacro.getBodyRenderMode());
    }

    @Test
    public void testMacroGeneratesHtmlBlockContent() {
        assertFalse(columnMacro.isInline());
    }

    @Test
    public void testMacroNameIsSection() {
        assertEquals("column", columnMacro.getName());
    }

    @Test
    public void testSurroundingTagNotSuppressedDuringWysiwygRendering() {
        assertFalse(columnMacro.suppressSurroundingTagDuringWysiwygRendering());
    }

    @Test
    public void testMacroRenderingSuppressedDuringWysiwyg() {
        assertTrue(columnMacro.suppressMacroRenderingDuringWysiwyg());
    }

    @Test
    public void testMacroHasBody() {
        assertTrue(columnMacro.hasBody());
    }
}
