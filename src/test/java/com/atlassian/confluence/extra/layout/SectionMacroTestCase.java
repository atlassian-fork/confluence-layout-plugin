package com.atlassian.confluence.extra.layout;

import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.macro.MacroException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static com.atlassian.renderer.v2.RenderMode.ALL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SectionMacroTestCase {
    private SectionMacro sectionMacro;

    @Mock
    private RenderContext renderContext;

    private Map macroParameters;

    @Before
    public void setUp() {
        sectionMacro = new SectionMacro();
        macroParameters = new HashMap();
    }

    @Test
    public void testRender() throws MacroException {
        final String body = "<div class=\"columnMacro\">Macro body text</div>";

        when(renderContext.getOutputType()).thenReturn("view");

        final String expected =
                "<div class=\"sectionColumnWrapper\">" +
                        "<div class=\"sectionMacro\">" +
                        "<div class=\"sectionMacroRow\">" +
                        "<div class=\"columnMacro\">Macro body text</div>" +
                        "</div>" +
                        "</div>" +
                        "</div>";
        final String actual = sectionMacro.execute(macroParameters, body, renderContext);
        assertEquals(expected, actual);

        verify(renderContext, times(1)).getOutputType();
        verify(renderContext, times(0)).isRenderingForWysiwyg();
    }

    @Test
    public void testRenderWithContentBeforeColumn() throws MacroException {
        final String body = "<p>I'm before the column!</p><div class=\"columnMacro\">I'm in the column!</div>";

        when(renderContext.getOutputType()).thenReturn("view");

        final String expected =
                "<div class=\"sectionColumnWrapper\">" +
                        "<div class=\"sectionMacro\">" +
                        "<p>I'm before the column!</p>" +
                        "<div class=\"sectionMacroRow\">" +
                        "<div class=\"columnMacro\">I'm in the column!</div>" +
                        "</div>" +
                        "</div>" +
                        "</div>";
        final String actual = sectionMacro.execute(macroParameters, body, renderContext);
        assertEquals(expected, actual);

        verify(renderContext, times(1)).getOutputType();
    }

    @Test
    public void testRenderWithBorder() throws MacroException {
        final String body = "<div class=\"columnMacro\">Macro body text</div>";

        when(renderContext.getOutputType()).thenReturn("view");

        macroParameters.put("border", Boolean.TRUE.toString());

        final String expected =
                "<div class=\"sectionColumnWrapper\">" +
                        "<div class=\"sectionMacroWithBorder\">" +
                        "<div class=\"sectionMacroRow\">" +
                        "<div class=\"columnMacro\">Macro body text</div>" +
                        "</div>" +
                        "</div>" +
                        "</div>";
        final String actual = sectionMacro.execute(macroParameters, body, renderContext);
        assertEquals(expected, actual);

        verify(renderContext, times(1)).getOutputType();
    }

    @Test
    public void testBodyIsRenderedToHtml() {
        assertEquals(ALL, sectionMacro.getBodyRenderMode());
    }

    @Test
    public void testMacroGeneratesHtmlBlockContent() {
        assertFalse(sectionMacro.isInline());
    }

    @Test
    public void testMacroNameIsSection() {
        assertEquals("section", sectionMacro.getName());
    }

    @Test
    public void testSurroundingTagNotSuppressedDuringWysiwygRendering() {
        assertFalse(sectionMacro.suppressSurroundingTagDuringWysiwygRendering());
    }

    @Test
    public void testMacroRenderingSuppressedDuringWysiwyg() {
        assertTrue(sectionMacro.suppressMacroRenderingDuringWysiwyg());
    }

    @Test
    public void testMacroHasBody() {
        assertTrue(sectionMacro.hasBody());
    }
}
