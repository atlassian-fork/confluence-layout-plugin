package com.atlassian.confluence.extra.layout.xhtml;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.renderer.v2.macro.MacroException;

import java.util.Map;

public class SectionMacro implements Macro
{
    private static final com.atlassian.confluence.extra.layout.SectionMacro INSTANCE = new com.atlassian.confluence.extra.layout.SectionMacro();

    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException
    {
        try
        {
            return INSTANCE.execute(parameters, body, conversionContext != null ? conversionContext.getPageContext() : null);
        }
        catch (MacroException e)
        {
            throw new MacroExecutionException(e);
        }
    }

    public BodyType getBodyType()
    {
        return BodyType.RICH_TEXT;
    }

    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }
}
